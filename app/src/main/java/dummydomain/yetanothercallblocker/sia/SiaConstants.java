package dummydomain.yetanothercallblocker.sia;

public interface SiaConstants {

    String SIA_PATH_PREFIX = "sia/";
    String SIA_SECONDARY_PATH_PREFIX = "sia-secondary/";

}
