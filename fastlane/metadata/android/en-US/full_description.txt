A simple call blocking application that will help you avoid answering unwanted calls
using an offline phone number database.

Features:

* Uses offline database.
* Blocks unwanted calls automatically (option).
* Displays a notification with phone number summary (rating, reviews count, category) during incoming calls (option).
* Automatic incremental/delta database updates (option).
* You can view online reviews for caller's number (provided by 3rd party service).

How to use:

* Install and launch the app.
* The option to show caller info notifications is enabled by default, so the app will ask for phone-related permissions.
* At first start, the app will suggest to download main database which is required for most of the functions. Simply confirm the download.
* Check the "Auto-update database" checkbox in the menu (recommended) to automatically receive daily DB updates (these are incremental/delta updates, so they consume very little traffic).
* Check the "Block unwanted calls" checkbox to block calls with negative rating automatically.
* You can also check the "Use contacts" checkbox if you want your contacts not to be treated as unknown callers.
* After these steps everything should just work. Enjoy!

The main phone number database is downloaded from a gitlab repository.
Delta updates and detailed review queries are performed using 3rd-party servers.
No user-identifiable information is sent (except for the phone number
the detailed reviews are loaded for).

Yet Another Call Blocker is still under development. Any help is very welcome.

''NonFreeNet:'' the 3rd-party service is not open source.
